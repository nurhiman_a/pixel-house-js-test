function sortAsc(arr) {
  let sortedArr = arr
  let arrLength = sortedArr.length
  for (let i = 0; i < arrLength - 1; i++) {
    for (let j = 0; j < arrLength - i - 1; j++) {
      if (arr[j] > arr[j+1]) {
        let setValue = arr[j]
        arr[j] = arr[j+1]
        arr[j+1] = setValue 
      }
    }
  }
  return sortedArr
}

function alphanumericSortAsc(arr) {
  let resultArr = []
  let arrNumber = []
  let arrString = []
  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] === "number") {
      arrNumber[arrNumber.length] = arr[i]
    } else {
      arrString[arrString.length] = arr[i]
    }
  }

  let sortArrNumber = sortAsc(arrNumber)
  let sortArrString = sortAsc(arrString)

  for (let j = 0; j < sortArrNumber.length; j++) {
    resultArr[resultArr.length] = sortArrNumber[j]
  }
  for (let k = 0; k < sortArrString.length; k++) {
    resultArr[resultArr.length] = sortArrString[k]
  }

  return resultArr
}

const numeric = [10, 9, 102, 66, 5421, 1, 0]
const stringArr = ["Wulan", "Raharjo", "Widya", "Yuda", "Cinta", "Iskandar", "Hidayat", "Kusuma", "Indah", "Jusuf"]
const alphanumeric = ["Wulan", "Raharjo", "Widya", 10, 9, 102, 66, 5421, 1, 0,"Yuda", "Cinta", "Iskandar", "Hidayat", "Kusuma", "Indah", "Jusuf"] 

console.log(sortAsc(numeric))
console.log(sortAsc(stringArr))
console.log(alphanumericSortAsc(alphanumeric))
