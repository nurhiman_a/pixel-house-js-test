function randomCharacter(length) {
  let resultCharacter = ''
  const characters = 'abcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for ( let i = 0; i < length; i++ ) {
     resultCharacter += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return resultCharacter
}

function randomPolindrom(length) {
  let character = randomCharacter(length / 2)
  let resultPolindrom = character
  if (length % 2 == 0) {
    for (let j = character.length - 1 ; j >= 0; j--) {
      resultPolindrom += character[j]
    }
  } else {
    for (let j = character.length - 2 ; j >= 0; j--) {
      resultPolindrom += character[j]
    }
  }
  return resultPolindrom
}

console.log(randomPolindrom(5))
